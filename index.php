<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Main</title>
</head>
<body>
    <h1>Berlatih OOP PHP</h1>
    <?php
        require_once ('animal.php');
        require_once ('frog.php');
        require_once ('ape.php');

        $sheep = new Animal("shaun");
        echo "Name : $sheep->name <br>";
        echo "Legs: $sheep->legs <br>";
        echo "Cold blooded: $sheep->cold_blooded <br><br>";
        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), 

        //frog
        $kodok = new Frog("buduk");
        echo "Name : $kodok->name <br>";
        echo "Legs: $kodok->legs <br>";
        echo "Cold blooded: $kodok->cold_blooded <br>";
        echo "Jump: ";
        $kodok->jump();
        echo " <br><br>"; // "hop hop"

        //ape
        $sungokong = new Ape("kera sakti");
        echo "Name : $sungokong->name <br>";
        echo "Legs: $sungokong->legs <br>";
        echo "Cold blooded: $sungokong->cold_blooded <br>";
        echo "Yell: ";
        $sungokong->yell(); 
        echo "<br><br>"; // "Auooo"

    ?>

</body>
</html>